During development, writing commit messages is something that is done so frequently that it shouldn't take much time, especially when there is a metric ton of atomic commits waiting to be submitted.

Yet writing good commit messages is hard.  

I've seen and used a number of different styles, but each had its flaws.
I'm going to expose and comment some of them, and then try to propose one that might be a good starter.

# Requirements
A commit message should be:

* short and concise: it must explain what change happened
* parsable: either by a machine or tools like grep, to allow the information being retrieved afterwards
* detailed about why those changes happened
* fast to write

Yes, there are contradictions.
Let's start with what these requirements entail:

## Short and concise
In 50 characters or less, it should give a summary of what change happened.
This allows humans to fly over a list of commits when looking for bug candidates, feature integration candidates, etc.

Anything longer will also take longer to read, ergo make any subsequent task longer.
And anything less concise can lead to features not being integrated, or buggy changes being ignored when binary searching for what change introduced a bug.

## Parsable
By obeying to a standard message style, the commit messages can be easily searched, e.g. via grep.
Also a number of bug/task tracking software tools integrate with the VCS by parsing the commit messages for specific tags.

E.g.
Redmine can group commits related to the same bug via the bug-id in the commit message.
JIRA goes even further and can add task comments, update the time spent on the task by parsing specific hashtags in the commit message.

## Detailed
This contradicts the short part, but ideally a commit message contains a detailed explanation of what the change does, and why.
Having this sort details will make it easier when merging pull-requests or even when doing code reviews.

## Fast to write
This contradicts the previous points, but when you have a number of atomic changes that you want to commit after finishing writing or refactoring some code, you don't want to spend the remainder of the day (or do overtime) writing commit messages.

# Styles
## Symbolic shorthand style
I've encountered the 'symbolic shorthand style' in several companies I worked for.
It works by using a set of symbols for each line of the commit message.

Examples:
`!A added foobar.h`  
`!D deleted foobaR.h`  
`!M moved foobar.h to src/`  
`!R renamed foobar.h to MyClass.h`  
`!I integrated @123456 from branch 'dev'` (note: Perforce marks changelists with `@<number>`)  
`!F fixed bug #12345`  
This looks concise until someone makes mistakes
`!A added feature`  
`!F completed feature`
and until other people replicate these format mistakes since it's confusing.

`+ added foobar.h`  
`- removed foobaR.h`  
`- implemented class Foobar`  
`+ added interface IFoo`  
`! important notice`  
This is even worse, as the number of allowed symbols is too limited to be expressive.

I think it's clear that this started from a good idea, only the reality of development lead to the need for many more symbols, which in turn becomes confusing. (And let's be honest, who would want to have to lookup a styleguide at every commit?).

Also, the symbol duplicates the meaning of the following verb, which is superfluous, or worse inconsistent.

## Semantic verbose style
This style consists of using a number keywords, mostly verbs, to illustrate the actions contained in the commit.
Since it's based on a given language, the meaning is pretty clear, and it does create any inconsistency with a symbol. 

Only it's verbose, and verbosity can be abused.
`added feature`
`implemented feature`
`fixed bug`
`changed one line`
`renamed A to B`
`fixed whitespace` (why?!)



 

