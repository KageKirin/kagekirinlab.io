Follow up of my [last post](./using-google-repo-in-game-production) on managing multiple repositories for game production.

Although I use game production as example, the tools might very well work as well for 
app development, classic software development, etc.

To continue where we left off.

# The strange case of Google Repo on Windows

The experience of using Google Repo on Windows was -- let's put it gently -- unsatisfactory.

Google Repo worked, but only when used inside Cygwin after sacrificing a virgin on a new moon. So, besides running out of virgins to sacrifice, the way Cygwin handles the symlinks that Google Repo is creating, makes the resulting git clones
incompatible with the majority of Git clients on Windows. (Incompatible in a very crashy way).

This hampering the real multiplatform development experience made me opt for another (Google produced) software development tool, namely _gclient_ from the Depot_Tools package.

Before continuing on this tool, let me rant a bit.


# The alternatives

What would be the alternatives to using  Google Repo in a multiplatform development scenario?
There's some choice:

## Git submodules

Git submodules are an excellent way to  tackle sparse repository management (aka multi-repository management, but this appellation sounds more professional).

The `git submodule` command is integrated to git-core and as such, works out of the box on any system supporting git.
Submodules also don't require any server changes, and in fact, GitHub, BitBucket et al. allow to navigate between modules in their web frontend.
Heck, even Heroku is fetching submodules when you push a project to their server. (I.e. the best way to add themes to a Ghost blog like this one).

So what are the cons?

While Git Submodule let's you easily _add_ new submodules via the `git submodule add` command, modifying existing submodules' remote URL or removing them altogether is a completely different task: in order to avoid removing submodules mismanipulation, the task has to be explicitely done by hand-editing several files to remove the references (`.gitmodules` and `.git/config`) before `git rm --cached` the path altogether.

Also, updating submodules requires you to run several commands, not only `git submodule init && git submodule update`, but a `git pull` in said submodule's folder, followed by a `git add`, `git commit` on the parent repository. Same recursion for committing changes.

So, while not entirely bad, this adds quite a layer of commands to run for a simple change.

[Reference](https://chrisjean.com/git-submodules-adding-using-removing-and-updating/)


## Git extensions (subtree, subrepo)

`Git subtree` has apparently made it to the core as well. Its workflow differs from submodule, but equally quite some commands to update or pushing upstream. [Reference](http://blogs.atlassian.com/2013/05/alternatives-to-git-submodule-git-subtree/).

`Git subrepo` is not part of the core, and has to be installed apart.
Its workflow is pretty similar to git subtree, but honestly, I never managed to push any change back into the upstream.
[link](https://github.com/ingydotnet/git-subrepo)

## Cloudera Repo

[Cloudera Repo](https://github.com/cloudera/crepo) is yet another Google Repo-like tool, using JSON instead of XML, but apparently not much in development anymore, as its latest commits date back to 6 years ago...


Hence the choice for Depot_Tools was a rather easy one...

# Google Depot_Tools

Chromium and Chromium OS use a package of scripts called [depot_tools](https://www.chromium.org/developers/how-tos/depottools) to manage checkouts and code reviews. The depot_tools package includes `gclient`, `gcl`, `git-cl`, `repo`, and others.
The tool we're interested in is called `gclient`.

## Installation

Since there's homebrew package, we will simply clone the git repo as indicated in its [installation guide](https://www.chromium.org/developers/how-tos/install-depot-tools).

I recommend, however, NOT TO put the package's path into your $PATH, but to use this minimal wrapper script instead.

```
#!/bin/bash
echo "wrapping command '$@' to depot_tools/gclient"
former_path=$PATH
PATH=~/Documents/Code/tools/depot_tools/:"$PATH" ## adapt this to your depot_tools installation path
gclient "$@"
PATH=$former_path
```

(Actually, it _might_ make even sense to add [the repo](https://chromium.googlesource.com/chromium/tools/depot_tools.git) as a submodule to your base repo, just so it can be deployed easily...).

Also, if anyone wants to take up the task to separate `gclient` from its package, it'll make my day.

## Initial setup

Running `gclient config` will create a configuration file `.gclient`.
** CAVEAT: This will also overwrite any existing `.gclient` file.**

This `.gclient` file manages the 1st order dependencies, namely the repos we will want to work on. Also, it's a Python file.

Let's consider the setup we used with Google Repo in the previous post:

- 2 working repos, `src` and `scaffolding`, hosted on our corporate Git server.
- 2 dependencies, of which on our corporate server, and one on Github.


Our working repos are 1st order dependencies, hence to be added into the `.gclient` file as follows:


```
solutions = [
  {
    "name"        : "src",
    "url"         : "git@git.corporate.local:kk/my-project-src.git",
    "deps_file"   : "DEPS",
    "managed"     : True,
    "custom_deps" : {},
  },
  {
    "name"        : "scaffolding",
    "url"         : "git@git.corporate.local:kk/my-project-scaffolding.git",
    "deps_file"   : "DEPS",
    "managed"     : True,
    "custom_deps" : {},
  },
]
cache_dir = None
```

Refer to the [documentation](https://www.chromium.org/developers/how-tos/depottools) for details,
but to put it simple:

- `name` defines the path where the git repo will be cloned to
- `url` is the full URL. HTTPS, Git and SSH protocols are supported
- `deps_file` indicates the name of the dependency referring file INSIDE the repo


The `DEPS` file manages 2nd order dependencies. As such, for example, 3rd party libraries we need to build our software product.
It's also a Python file.

In our case, we have the following `DEPS` file inside our `src` repo to handle its dependencies:

```
vars = {
	"awesome_revision": "f2f162e34d7b024e0de79c55133fef00e82fffe0",
	"flatbuffers_revision": "2fdafa9a4920f103e33e29f72960db6e35a63d4e",
}

deps = {
	"src/thirdparty/awesome":
		"git@git.corporate.local:thirdparty/awesome.git@" + Var("awesome_revision"),
	"src/thirdparty/flatbuffers":
		"https://github.com/google/flatbuffers.git@" + Var("flatbuffers_revision"),
}
```

The dict `vars` is used to indicate the revisions to check out, and the dict `deps` references our dependencies,
consisting of their clone path, repo URL and the revision suffix constructed by the `Vars()` function.


Once we have this setup (and safely commit-pushed the DEPS file to the remote), we can run the initial checkout by calling
`gclient sync`.


## Subsequent updates

Subsequent updates will consist of calls to `git pull` on the master repo to fetch optional `.gclient` changes, followed by `gclient sync` to fetch the other repos.

Branching inside the master repo allows to add optional working branches that might want to integrate back into the master once the feature is stable enough.

CAVEAT: the checked out working repos will first be checked out at `HEAD`, therefore requiring a `git checkout master` so subsequent commits don't get lost.
(In which case you can always retrieve the lost commits by checking the reflog and `git cherry-pick` ing the lost revisions into your current branch).



# Unity projects

Here's a port of the Google Repo manifest used in the previous post.

```
solutions = [
  {
	"name": "ProjectSettings",
	"url":"git@git.corporate.local/kk/MUG-settings.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
  {
	"name": "Assets/Src/Gameplay",
	"url":"git@git.corporate.local/kk/MUG-gamplaycode.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
  {
	"name": "Assets/Shaders",
	"url":"git@git.corporate.local/kk/MUG-shaders.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
  {
	"name": "Assets/Art",
	"url":"git@git.corporate.local/kk/MUG-artassets.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
  {
	"name": "Assets/Levels",
	"url":"git@git.corporate.local/kk/MUG-levelassets.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
  {
	"name": "Assets/Controllers",
	"url":"git@git.corporate.local/kk/MUG-controllerassets.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
  {
	"name": "Assets/Editor Assets",
	"url":"git@git.corporate.local/kk/MUG-editor.git",
	"deps_file": "DEPS",
	"managed": True,
	"custom_deps": {}
  },
]
cache_dir = None
```

The logic for separating the repos has not changed, only the tool to put them together has.


# Conclusion or tl;dr

Depot_Tools `gclient` works well on all tested platforms (limited to macOS and Windows for the moment), but there's no reason why it shouldn't work elsewhere, the tools being based on Python.

Although the documentation recommends to add the depot_tools installation in front of your $PATH variable, so to avoid naming clashes between `gcl` and the GNU Common Lisp compiler, I found that it created me a naming clash between the `clang-format` bundled with depot_tools (which is only compatible with Chromium code somehow), and my homebrew-managed `clang-format`. In fact, I'd recommend everyone to just adapt and use the small bash script I cited in this article.

So far as my opinion goes, I'm pretty happy with gclient for now, and I think I will keep this tool around for the next few projects.

Also, use **sparse repository management** to refer to multiple repositories used for 1 project.
