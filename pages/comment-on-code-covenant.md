My comments on the [Code Covenant](code-covenant.html)

# Comments on the Code Covenant

I came up with this simple worded covenant after reading
about Linus Torvalds (temporarily?) stepping down from his terminal emulator project,
and the backstory to this happening.


## About rule #1 ("don't be an arse")

I think we all have already enough negative people to deal with in day-to-day/work projects,
so there's absolutely no need to have to deal with such persons in personal projects,
on my free time.

Hence the requirement for all contributors to not impersonate their southpole,
and stick to some common curtesy.



## About rule #2 ("no ideology")

This part is more complicated to explain.
Yes, all the "ideological" issues this rule excludes from discussion exist.
But no, in my humble opinion, their place for discussion is not to be part of
an open source, or free-time project.

Especially, not inside the issues, commit messages or pull requests.
In fact, sticking to the pure technical issues of the project can be considered
the most neutral form of communication:
technology has no preferences, it only works as expected, or not.

Furthermore, I don't think any of the projects I use the code convenant on,
has reached the level of utmost primordial importance that discussing
social matters inside its meta would have a beneficial effect for it.

## Sum up

So, since rule #1 excludes insults and rule #2 excludes discussion of social matters
outside of the scope of the project, I think we are well prepared for successful collaboration.
