This describes the approach to manage several medium sized C++ projects and their dependencies.

## Projects

During my (still on-going) tenure at DeNA, I've managed and worked on 2 projects: an asset converter for a legacy 3D model/animation format into FBX and glTF, and a new renderer prototype for said legacy asset data targeting WebGL via Emscripten.

Since the context of these projects relate to on-going services of my employer, I won't (can't) go into details about the actual implementations, but rather cover one aspect of managing WIP software project and their respective set of dependencies (C++ libs, tools, ...).

## Key issues

- 1 man-team
  - not invented here: FOSS libs to the rescue
- Dependency hell
- Server-side usage to be kept possible
- Multiplatform
- Emscripten: build from source is must.
- Re-use: multiple projects, common scaffolding

### Side issue: compilation time


## Scaffolding (aka one hammer for many nails)

### gclient and DEPS
### GENie project luafiles
### makefile as command hub
### scaffold 

### current structure