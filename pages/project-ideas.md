In which I'm trying to track a couple of personal project ideas.


# Project ideas

## Metal API wrappers

* _metallic_: aka "METAL Lib In C", a C wrapper for Metal.
  relatively straightforward, just expose Obj-C Metal functions to C

* _Mithril_: a Metal-on-Vulkan implementation, C
  basically continuing on the above, expose a C-API similar to Metal
  implement backends using Metal and Vulkan
  goal: simpler and portable graphics interface

* _Alloy_: or _AlloyVM_, a LLVM-based API language that gets
  ~~transpiled~~ compiled into direct API calls for Metal or Vulkan
  goal: single source to generate direct API-bound calls


## glTF libs

* _flaxGLTF_: a follow up on FlatGLTF, but more extensible.
  I.e. customizable for a super/sub-set of the spec with a number of extensions.
  Also, flatbuffer-based binaries for loading without parsing

* _CapnGLTF_: same as the above, only relying on CapnProto as schema IDL and C++ backend

* _yasGLTF_: again, glTF loader, this time based on _yas_
* _jsmnGLTF_: glTF loader relying on _jsmn_
* _pjsonGLTF_: glTF loader relying on _pjson_

* the big idea here: pitch those different implementations against each othher to find the fastest
  and lowest overhead possible

## glTF extensions

* XX_Material_External_Reference: materials reference external materials in other glTF file
  also enable _pure_ material libraries that could be referenced by models
* XX_Technique_SPIRV: like KHR_Technique_WebGL, but containing SPIR-V compiled shaders
* XX_Technique_SMOLV: like KHR_Technique_WebGL, but containing SMOL-V compiled shaders
* XX_Zeux_MeshCompression: like KHR_Draco_MeshCompression, but using @Zeuxcg mesh optimizer lib instead
