A simple straightforward covenant for code contributors.

# Code Covenant

By committing code / time / patches / bug reports
to this project,
you agree to the following rules:

1. Don't be an arse
2. No ideology

If you are not able to agree to these simple rules,
please leave this project and its committers alone.

-- signed: The Maintainer


## 1. Don't be an arse

This rules indicates that you should follow normal social behaviour:

### don't insult people

e.g. "You dipshit of a motherfucking retard" is not acceptable.


### give constructive feedback

- bad: "this is crap"
- better: "this is crap because <technical sound and verifiable arguments>"
- even better: "I don't like this because <reasons related to personal use case>"


### no entitlement

also no personal feelings.

### respectfully disagreeing with the maintainer is ok

but he will have the last word.

### stay focussed on what relates to this project



## 2. No ideology

This rule indicates that ideological matters don't have their place in this project.

### no politics
### no religion
### no social justice warfare
### no discussing of sexual orientation or gender

### refusal of contribution in case of no-abidance

Your contribution will get refused if you violate any of the above rules on a platform
related to the project (repository, repository host, linked services).

### refusal of contribution

If your contribution gets refused, it will be because of purely technical reasons,
and not beause of your looks / skin color / religion / gender / or whatsoever.

### reasons for banning

Discussing any of the above matters in the issues, documentation or commit messages
will get ejected of the project.



