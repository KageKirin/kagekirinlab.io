Disclaimer: *I have worked on several projects involving large teams using CVS, Perforce, Subversion and Git, my longest experience having been Perforce which might have shaped my views.*

The choice of a versioning system as well as its configuration will heavily impact how a team collaborates, produces tangible outcome and gets stuff done.
This obvious fact gets all to often forgotten when collaboration tools such as a versioning system gets imposed by company policy.
Rather than ranting about this, I'd like to share a specific setup I have been using in the small for the last 6 months, of which I am positive that it might work well in a larger environment.

But first, let's start with the introductions.

# WTF is Google Repo?

According to its [project page](https://code.google.com/p/git-repo/), Google Repo is
"multiple repository tool [...] built on top of Git [...] to make it easier to work with Git in the context of Android".
In short, it's a Python script to handle Git when using multiple repositories, used by the Android project.

You may think of Android what you want, the point is, this tool can come in very handy when using multiple repositories.


# Why would I want to use multiple repositories? Would not one suffice?

This is where I have to explain from background knowledge.
When working in a larger team on a game, you have many actors working on different aspects in parallel, and hopefully in collaborative ways.
For example:
- The programmers are working on the source code. Depending on complexity and features, they might split off work by specialization, e.g. graphics/rendering, sound, gameplay, network, 
  and work on branches to avoid collapsing the card house (once, shortly before each milestone should be enough...)  
- The artists (modelers, texturers, sound designers, level designers, animators,...) are working on creating assets. While assets might have several revisions, there is no need for branching.
  Oh, and assets can be considered to be large binary files, that are hardly fit for branching as they can't be merged.  
- (For simplification, I omit the other actors that have less use for a versioning system).  
- Also, for releases (pre-alpha to final milestones, QA test sets, etc), we need to be able to create snapshots of the repository, in case we need to reproduce a specific version later in order to track down a hard to find bug.

As we can see, we run into a large dilemma when opposing those needs: large binaries that can't be merged and only need a rollbackable backup in case of messup VS mostly text files (and some binary dlls or libs) that require heavy branching and easy mergeability. And on top, we need to create full snapshots that can be easily retrieved.
Also, since we're talking about team, the programmers will need the assets to start the game in debug, whereas the artists will need at least a versioned, binary release of the game (or the main branch of source code, in some cases).

So, to match these requirements, we will need:
- 1+ source repositories, with many branches, merges, etc  
- 1+ dependency repositories for 3rd party libraries that are not developed in-house, and might come as Header+Libraries  
- 1+ asset repositories containing large assets, but in one linear branch.

And that's only for 1 game. Having several games in the pipeline, re-using different features/assets from another, complexifies this setup.

Point made, 1 single repository would be a clusterfuck to maintain and impeach each actor's and the whole team's productivity.

*In a Perforce world, as a matter of fact, you might indeed have only one "repo", but with many branches mapped together via Perforce's **workspace-side branch-mapping**.
The idea I'm trying to convey here is to simili-reproduce the **branch-mapping** in the (unfortunate?) case of not using Perforce.*

## Branch mapping

I think I need to quickly explain how Perforce works. Although I think the official website does a better job at this, let me just put out the facts and proceed.

On server side, Perforce repo consists of a large file/folder tree containing our files and their revisions. (This is oversimplified, but it's a cognitive model that's easy to understand). Branching works by "copying" a folder branch of said tree. (It's not copying, more like smart referencing the original's branch history, but for our file system model, let's consider the files will be available from the new folder as well, with a branched off history).

On client side, we have to set up a *workspace* that mainly consists of text file that explains how (or rather, where) the repo folders are going to be **mapped** onto the local filesystem when performing a *sync*.
This branch mapping file consists of 2 paths per line, (server) repo-side path, and local filesystem path, e.g.
```
//depot/projectA/src/... //kk-projectA-dev1/src/...
//assets/projectA/assets/baked/... //kk-projectA-dev1/assets/...
//depot/thirdparty/awesomelib@211245/... //kk-projectA-dev1/src/thirdparty/awesomelib/...
```

(For simplicity's sake, I'm wilfully ignoring some other useful features from Perforce in this example, namely additive and subtractive mapping).

Explanation:

Line 1: I'm mapping the full contents (folder and recursively subfolders) of the server-side path `//depot/projectA/src/` to my local path `//kk-projectA-dev1/src/...`.
On server side, our repo is called "depot" (there might be more than 1 involved), and the actualy path goes to a branch called `projectA`, with a subfolder `src`.
On client side, my workspace is called "kk-projectA" (prefix "kk-" and suffix "-dev1" as workspace names must be unique throughout the server), and I want the files to go into a subfolder called `src`. This is straightforward mapping.

Line 2: I'm mapping the full contents of a path from a different repo into my workspace. In this case, it was decided that artists have their own repo.
Since source assets are not particularly useful to me (as a programmer, I have no need to fill my drive with large PSDs or FBXs), I take the subfolder `baked` which content gets updated through an automated asset build.

Line 3: We have licensed some "awesome library" for our game, and put its releases into a different branch on our server. Since I need a specific version of that library, I fixed the revision to checkout with `@` and its changelist (commit) number. Our project layout expects to find the header+libraries inside `src/thirdparty/awesomelib`.

(It has been a while since I last used Perforce. Surprising how I can still pour out a simplified explanation).

*Note that Perforce's **Git Fusion** interfaces also uses branch mapping to expose a workspace as a git repo.*

The really interesting stuff follows now:


# Setting up Google Repo for use with our project

## Getting Repo

Getting Repo is [pretty easy](http://source.android.com/source/downloading.html#installing-repo): there's git repo to fetch (alternatively, a gzipped tarball) and to add to the path.  

On macOS, it's even easier: `brew install repo`, done.  

On Windows, it's a different story, as repo relies on symlinks to move the data to the correct place. See the last section about the difficulties I encountered.

## Setting up the (initial) default manifest

The manifest is a XML file referencing the different git repositories you want to fetch, and mapping them to a local subfolder.

For our example, let's say the we store our repo on a corporate git server, located at git.corporate.local (it could be an instance of GitHub enterprise, for example).
This would be the manifest file, saved as `default.xml`:

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
	<remote
	 name="corporate"
	 fetch="ssh://git@git.corporate.local/" />
	<remote
	 name="github"
	 fetch="https://github.com" />	
	<default revision="refs/heads/master"
	 remote="github"
	 sync-j="4" />
	<project name="kk/my-project-src.git" remote="corporate" path="src"/>
	<project name="kk/my-project-scaffolding.git" remote="corporate" path="scaffolding"/>
	<project name="thirdparty/awesome.git" remote="corporate" path="src/thirdparty/awesome"/>
	<project name="google/flatbuffers.git" remote="github" path="src/thirdparty/flatbuffers"/>
</manifest>
```

We reference 2 remotes (git servers): our corporate local one, and GitHub since we need some OpenSource libs in our game.
We reference 4 projects (git repos) that bound together will form our workspace:
- `my-project-src` contains the source code.
- `my-project-scaffolding` contains the makefiles, meta-makefiles (premakue/lua), tools, etc...
- since we put awesomeLib on our local git server as well, we can reference its repo and map it into a subfolder `src/thirdparty`.
- and we take Google's flatbuffers project from GitHub, and map it into a subfolder of `src/thirdparty` as well.

Now that we have a valid manifest, we need to save, add, commit and push it to another git repo, that we'll put on `https://git.corporate.local/kk/my-project-manifest.git`.
This closes the initial setup part of the project.

To fetch the local copy to work on, we call `repo init -u https://git.corporate.local/kk/my-project-manifest.git` from a freshly created local folder to init the workspace.
Once the initialization finishes, we can get the files by calling `repo sync`.

Repo integrates other command to commit, see the status, etc of the projects.
At this point, we just need to switch each project to the master branch so we can start working. This works by calling `repo start master` or `repo checkout master`.
(Or going to each cloned git repo and calling `git checkout master` individually).


# A more concrete example

I.e. my setup for a FBX converter.  
(The project is called BADASS, standing for Basically Almost Done ASSet converter, a name which I consider still miles above "AssImp" (the Open Asset Import Library)).

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
	<remote
	 name="denagh"
	 fetch="ssh://git@github.dena.jp/" />
	<remote
	 name="github"
	 fetch="https://github.com" />
	<default revision="refs/heads/master"
	 remote="denagh"
	 sync-j="4" />
	<project name="christian-helmich/badass-src" remote="denagh" path="src"/>
	<project name="christian-helmich/badass-scaffolding" remote="denagh" path="scaffolding"/>
	<project name="christian-helmich/Autodesk_FBX_SDK.git" remote="denagh" path="contrib/fbxsdk"/>
	<project name="christian-helmich/Autodesk_FBX_Extension_SDK.git" remote="denagh" path="contrib/fbxextsdk"/>
	<project name="christian-helmich/boost.git" remote="denagh" path="scaffolding/thirdparty/boost"/>
	<project name="christian-helmich/glm.git" remote="denagh" path="scaffolding/thirdparty/glm"/>
	<project name="christian-helmich/cppformat.git" remote="denagh" path="scaffolding/thirdparty/cppformat"/>
	<project name="christian-helmich/pthread-win32.git" remote="denagh" path="scaffolding/thirdparty/pthread-win32"/>
	<project name="christian-helmich/bandit.git" remote="denagh" path="scaffolding/thirdparty/bandit"/>
	<project name="joakimkarlsson/snowhouse.git" remote="github" path="scaffolding/thirdparty/bandit/bandit/assertion_frameworks/snowhouse"/>
	<project name="google/flatbuffers.git" remote="github" path="scaffolding/thirdparty/flatbuffers"/>
	<project name="ignatz/pythonic.git" remote="github" path="scaffolding/thirdparty/pythonic"/>
	<project name="christian-helmich/cpplinq" remote="denagh" path="scaffolding/thirdparty/cpplinq"/>
</manifest>
```


In this project, all the thirdparty libraries reside under `scaffolding/thirdparty`, along with their respective GENie lua files.
The only exception is Autodesk's FBX SDK, which I want to be located under `contrib` to make it easier to open source this project at one point (knowing that I can't publish Autodesk FBX SDK on GitHub myself).


# Another semi-rhetoric example using Unity

A typical Unity project folder has the following structure:
- `ProjectSettings` contains as its name indicates, project settings. Those will be set once, and mostly not change much for the rest of dev.  
- `Assets` contains the game assets, source files, Unity metadata files (`.meta`), etc. The subfolder structure is completely project dependent.  
- `Editor Assets` contains mostly additional scripts used by the editor for more functionality.  
- `Library` contains local user files. Usually, this is not recommended to track.

If we want to split such a workspace by actors' responsibilities, we ought to keep
- `ProjectSettings` in a single repo, so changes can be easily tracked down to potential errors.  
- `Editor Assets` and its subfolders apart from the main source code, so to separate Editor features from actual game features  
- thirdparty Unity plugins outside of our code  
- a clear separation between asset files, especially external created assets (FBX models, PSD textures), Unity created assets (diverse controller setups, materials), gameplay source code, graphics source code (shaders) etc.

From this separation of concerns, our manifest file might well look like this:
(MyUnityGame shortens to MUG).

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
	<remote
	 name="corporate"
	 fetch="ssh://git@git.corporate.local/" />
	<remote
	 name="github"
	 fetch="https://github.com" />	
	<default revision="refs/heads/master"
	 remote="github"
	 sync-j="4" />
	<project name="kk/MUG-settings.git" remote="corporate" path="ProjectSettings"/>
	<project name="kk/MUG-gamplaycode.git" remote="corporate" path="Assets/Src/Gameplay"/>
	<project name="kk/MUG-shaders.git" remote="corporate" path="Assets/Shaders"/>
	<project name="kk/MUG-artassets.git" remote="corporate" path="Assets/Art"/>
	<project name="kk/MUG-levelassets.git" remote="corporate" path="Assets/Levels"/>
	<project name="kk/MUG-controllerassets.git" remote="corporate" path="Assets/Controllers"/>
	<project name="kk/MUG-editor.git" remote="corporate" path="Assets/Editor\ Assets"/>
</manifest>
```

This is just an example, though, and I recommend to try a number of possible configurations on some test projects before moving into production.
That said, I think this gives you an idea about how to split up your project for use with repo.


# The case of Windows and an outlook

tl;dr: "It doesn't work".

The above is only a half-truth. It does work under specific circumstances (namely, after offering the blood of a virgin on a new moon, but I digress).
The original Google Repo is confirmed not to work correctly under Windows by [StackOverflow](http://stackoverflow.com/questions/14223302/how-to-use-googles-repo-tool-on-ms-windows-os).

There is ["port" (or fork)](https://github.com/esrlabs/git-repo) that does work, although I only found it not to be crashing when run from a Cygwin shell.
In my test case (using the manifest detailed above), esrlabs-repo did indeed finish its job and produced a workspace, that I could use... from within Cygwin only.

It appears that Repo is creating symlinks between the workspace's `.repo` folder where the associated git repos get checked out and their actual mapped path.
Cygwin does this as well, only Cygwin's symlinks are not compatible with Windows' equivalent (Junctions),
and thus every Windows-based git-client expecting to find a regular `.git` folder, terminates with a beautiful crash or error message.
(It figures this is due to Cygwin handling symlinks with textfiles indicating where to find the original.)

So the option is to use repo-in-cygwin-on-windows, along with cygwin-git-on-windows, or not to use Repo.

I will write a follow post on my (current) final (?) choice: 
*SPOILER ALERT*
Google Depot Tools

# References

https://code.google.com/p/git-repo/  
http://www.instructables.com/id/Using-Googles-repo-command-in-your-own-projects/  
https://source.android.com/source/using-repo.html  
http://blog.bigpixel.ro/2013/04/using-googles-repo-tool-with-in-own-projects-on-bitbucket/



