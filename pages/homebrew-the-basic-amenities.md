Since I spent like 2 hours last night trying to figure why I couldn't install something in brew cask, I felt it necessary to write this post (mostly again, as a memo for myself).

# Prerequisites

* OSX
* Ruby (usually installed by default)
* curl (installed by default)
* XCode command line tools (installed on demand)

# Homebrew
The basic install command for [Brew](http://brew.sh/) is
`ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

This will take some time.
`brew doctor` to check that everything is OK.
`brew install wget` to install wget (e.g.)

# Cask
Install [Caskroom](http://caskroom.io/) with  
`brew install caskroom/cask/brew-cask`

`brew cask install google-chrome --appdir=/Applications` to install Chrome into /Applications (default would be /opt/homebrew-cask/Caskroom, with a symlink in ~/Applications).

# Usual installation
```
brew install git
brew install wget
brew install python
brew install ruby
brew install node

brew cask install caffeine --appdir=/Applications
brew cask install sublime-text --appdir=/Applications
brew cask install atom --appdir=/Applications
brew cask install p4merge --appdir=/Applications
brew cask install google-chrome --appdir=/Applications
brew cask install firefox --appdir=/Applications
brew cask install cyberduck --appdir=/Applications
brew cask install sourcetree --appdir=/Applications
``` 

# Updating
`brew update && brew upgrade --all`



